import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

import './DataCard.css'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

const DataCard = () => {
  const [dealCard, setDealCard] = useState([]);

  const { id } = useParams();
  let creationDate = undefined;
  let updatedDate = null;

  const loadDeals = async () => {
    try {
      const res = await axios.get(`/${id}`);
      setDealCard(res.data);
      console.log(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    loadDeals();
  }, []);

  creationDate = new Date(dealCard.createdAt).toLocaleDateString('nb-NO');
  updatedDate = new Date(dealCard.updatedAt).toLocaleDateString('nb-NO');

  return (
    <div className="form">

      <Card className="card">
        <CardContent>
          <Typography variant="h4" gutterBottom>
            Deal Details
            </Typography>
          <Typography >
            Created: {creationDate}
          </Typography>

          {updatedDate !== 'Invalid Date' ? (
            <Typography >
              Modified: {updatedDate}
            </Typography>
          ) : ('')}

          <Typography >
            Salesman: {dealCard.salesman}
          </Typography>
        </CardContent>
        <CardContent className="deal-card">
          <Typography variant="h5" gutterBottom>
            Information
              </Typography>
          <Typography>
            Consultant Name: {dealCard.consultantName}
          </Typography>
          <Typography>
            Broker: {dealCard.broker}
          </Typography>
          <Typography >
            End Customer: {dealCard.endCustomer}
          </Typography>
          <Typography >
            Price: {dealCard.price + ' €/h'}
          </Typography>
          <Typography >
            Allocation: {dealCard.allocation + ' %'}
          </Typography>
          <Typography >
            Payment Terms: {dealCard.paymentTerms}
          </Typography>
          <Typography >
            Other Terms: {dealCard.otherTerms}
          </Typography>
          <Typography >
            Starting Date: {dealCard.startingDate}
          </Typography >
          <Typography >
            Duration: {dealCard.duration}
          </Typography >
          <Typography >
            Contact person: {dealCard.contactPerson}
          </Typography >
        </CardContent>

        <CardContent>
          < Typography variant="h5" gutterBottom>
            Internal contractor information
            </Typography >

          < Typography >
            Sub Contractor Name: {dealCard.subContractorName}
          </Typography >
          <Typography>
            Price: {dealCard.price2 !== null ? dealCard.price2 + ' €/h' : ''}
          </Typography>
          <Typography >
            Other Info: {dealCard.otherInfo}
          </Typography >
        </CardContent>



      </Card >
    </div>
  );
};

export default DataCard;
