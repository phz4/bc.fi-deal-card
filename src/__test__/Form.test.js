import React from 'react';
import ReactDOM from 'react-dom';
import Form from './../Form/Form'

import { render, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

//Test the Form component
it("renders the component correctly", ()=> {
const { asFragment } = render(<Form text =" form"/>)
expect(asFragment()).toMatchSnapshot();
})

// Test the form without crashing
it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<form></form>, div)
})

