import React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { Link } from 'react-router-dom';
import VisibilitySharpIcon from '@material-ui/icons/VisibilitySharp';
import DeleteSweepRoundedIcon from '@material-ui/icons/DeleteSweepRounded';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import { green } from '@material-ui/core/colors';
import Tooltip from '@material-ui/core/Tooltip';
import { Container } from '@material-ui/core';

const DealTable = ({ data, onClick }) => {
  const rows = [];
  const columns = [
    { field: 'id', headerName: <strong> ID </strong>, width: 100, hide: true },
    { field: 'deals', headerName: <strong> Deals </strong>, width: 100 },
    {
      field: 'consultantName',
      headerName: <strong> Consultant Name</strong>,
      width: 200,
    },
    { field: 'salesman', headerName: <strong> Salesman</strong>, width: 170 },
    { field: 'broker', headerName: <strong> Broker</strong>, width: 170 },
    {
      field: 'endCustomer',
      headerName: <strong> End Customer</strong>,
      width: 170,
    },
    {
      field: 'paymentTerms',
      headerName: <strong> Payment Terms</strong>,
      width: 170,
    },
    {
      field: 'icons',
      width: 150,
      renderHeader: () => <strong>{'Actions '}</strong>,
      disableClickEventBubbling: true,
      sortable: false,
      renderCell: (params) => (
        <div>
          <Tooltip placement="top-start" title="View">
            <Link to={`/${params.data.id}`}>
              <VisibilitySharpIcon
                style={{ color: green[500] }}
                cursor="pointer"
              />
            </Link>
          </Tooltip>
          <Tooltip placement="top-start" title="Edit">
            <Link to={`/edit/${params.data.id}`}>
              <EditOutlinedIcon cursor="pointer" />
            </Link>
          </Tooltip>
          <Tooltip placement="top-start" title="Delete">
            <Link to="">
              <DeleteSweepRoundedIcon
                onClick={() => onClick(params.data.id)}
                color="secondary"
                cursor="pointer"
              />
            </Link>
          </Tooltip>
        </div>
      ),
    },
  ];

  data.map((items, index) => {
    return rows.push({
      id: `${items._id}`,
      deals: `${index + 1}`,
      consultantName: `${items.consultantName}`,
      salesman: `${items.salesman}`,
      broker: `${items.broker}`,
      endCustomer: `${items.endCustomer}`,
      paymentTerms: `${items.paymentTerms}`,
    });
  });

  return (
    <Container>
      <div style={{ display: 'flex' }}>
        <div style={{ flexGrow: 1 }}>
          <DataGrid
            autoHeight={true}
            rows={rows}
            columns={columns}
            pageSize={10}
          />
        </div>
      </div>
    </Container>
  );
};

export default DealTable;
