import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './NavBar.css'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


const NavBar = () => {
  const [searchInput, setSearchInput] = useState('');

  const searchHandler = (e) => {
    setSearchInput(e.target.value);
  };

  return (

    <AppBar position="static">
      <Toolbar>
        <Typography >
          <Link to="/" className="nav-link">
            <Button style={{ color: "white" }} >Home</Button>

          </Link>
        </Typography>
        <Typography>
          <Link to="/form" className="nav-link">
            <Button style={{ color: "white" }}>Add new deal card</Button>
          </Link>
        </Typography>
      </Toolbar>

    </AppBar>

  );
};

export default NavBar;
