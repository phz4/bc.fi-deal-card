import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import Modal from '@material-ui/core/Modal'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import './Form.css'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const Form = () => {
  let today = new Date().toLocaleDateString('nb-NO');
  const [newCard, setNewCard] = useState({
    date: today,
    salesman: '',
    consultantName: '',
    broker: '',
    endCustomer: '',
    price: '',
    allocation: '',
    paymentTerms: '',
    otherTerms: '',
    startingDate: '',
    duration: '',
    contactPerson: '',
    subContractorName: '',
    price2: '',
    otherInfo: '',
  });
  const [checkbox, setCheckbox] = useState(false);
  const history = useHistory();
  const [required, setRequired] = useState(false);
  const [isDisabled, setDisabled] = useState(false);
  const [dbEndCustomer, setDbEndCustomer] = useState([])
  const [open, setOpen] = useState(false)

  let {
    date,
    salesman,
    consultantName,
    broker,
    endCustomer,
    price,
    allocation,
    paymentTerms,
    otherTerms,
    startingDate,
    duration,
    contactPerson,
    subContractorName,
    price2,
    otherInfo,
  } = newCard;


  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
    history.push('/')
  }

  const checkboxHandler = () => {
    setCheckbox(!checkbox);
    setRequired(true);
  };

  const changeValueHandler = (e, value) => {
    if (typeof value == undefined || value == null) {
      setNewCard({ // if end customer is NOT chosen from the dropdown list
        ...newCard,
        [e.target.name]: e.target.value,

      })
    } else { // if end customer is chosen from the dropdown list
      setNewCard({
        ...newCard,
        [e.target.name]: e.target.value,
        endCustomer: value
      })
    }
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    setDisabled(true);
    handleOpen()
    console.log('popup open')
    console.log(newCard);

    await axios
      .post('/postDeal', newCard)
      .then((response) => {
        console.log('new card added');
        console.log(response.data);

      })
      .catch((error) => {
        console.log(error);
      });
  };

  const content = (
    <div className='overlay'>
      <div className='popup' >
        <h2>New deal card added!</h2>
        <button className='popup-btn' onClick={handleClose}>Ok</button>
      </div>
    </div>
  )

  const getEndCustomer = async () => {
    try {
      const res = await axios.get('/getAllDeals');
      console.log(res.data)

      const endCustomers = res.data.map(a => {
        console.log('all end customers' + a.endCustomer)
        return a.endCustomer
      })

      const endCustomerArray = endCustomers.filter((item, index) => endCustomers.indexOf(item) === index)
      console.log('array of end customers' + endCustomerArray)
      console.log(endCustomers.sort())
      const sortedArray = endCustomerArray.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase())
      })
      console.log(sortedArray)
      setDbEndCustomer(sortedArray)
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getEndCustomer();
  }, []);

  return (
    <div className="form">
      <Card className="card" >

        <form onSubmit={submitHandler}>
          <p className="is-required"> * = information is required</p>
          <div className="date-area">
            <Typography

              htmlFor="date"
              id="date"
              name="date"
              value={date}
              onSubmit={changeValueHandler}
            >
              Created Date: {date}
            </Typography>

            <Typography className="form-paper">
              <TextField
                type="text"
                value={salesman}
                name="salesman"
                onChange={changeValueHandler}
                required
                label="Salesperson name"
                variant="outlined"
                margin="normal"
                fullWidth
              />
            </Typography>

          </div>
          <div className="information-area">
            <Typography variant="h5">Information</Typography>
            <div>
              <TextField
                type="text"
                value={consultantName}
                name="consultantName"
                onChange={changeValueHandler}
                required
                label="Consultant name"
                variant="outlined"
                margin="normal"
                fullWidth
              />
            </div>
            <div>
              <TextField
                type="text"
                value={broker}
                name="broker"
                onChange={changeValueHandler}
                required
                label="Broker name"
                variant="outlined"
                fullWidth
              />
            </div>
            <div>
              <Autocomplete
                className="autocomplete"
                freeSolo
                options={dbEndCustomer}
                onChange={changeValueHandler}
                name="endCustomer"

                renderInput={(params) => (
                  <TextField {...params}
                    onChange={changeValueHandler}
                    name="endCustomer"
                    className="textfield"
                    value={endCustomer}
                    type="text"
                    variant="outlined"
                    label="End customer"
                    margin="normal"

                    required
                  />
                )}>
              </Autocomplete>
            </div>
            <div>
              <TextField
                type="number"
                step=".01"
                value={price}
                name="price"
                onChange={changeValueHandler}
                variant="outlined"
                label="Price (€/h)"
                fullWidth
                required
              />
            </div>
            <div>
              <TextField
                type="number"
                value={allocation}
                name="allocation"
                onChange={changeValueHandler}
                variant="outlined"
                label="Allocation (%)"
                margin="normal"
                fullWidth
                required
              />
            </div>
            <div >
              <TextField
                type="text"
                value={paymentTerms}
                name="paymentTerms"
                onChange={changeValueHandler}
                variant="outlined"
                label="Payment term"
                fullWidth
                required
              />
            </div>
            <div >
              <TextField
                type="text"
                value={otherTerms}
                name="otherTerms"
                onChange={changeValueHandler}
                label="Other terms"
                variant="outlined"
                margin="normal"
                fullWidth
              />
            </div>
            <div >
              <TextField
                type="text"
                value={startingDate}
                name="startingDate"
                onChange={changeValueHandler}
                variant="outlined"
                label="Starting date"
                fullWidth
              />
            </div>
            <div >
              <TextField
                type="text"
                value={duration}
                name="duration"
                onChange={changeValueHandler}
                variant="outlined"
                label="Duration"
                margin="normal"
                fullWidth
              />
            </div>
            <div >
              <TextField
                type="text"
                value={contactPerson}
                name="contactPerson"
                onChange={changeValueHandler}
                variant="outlined"
                label="Contact person"
                margin="normal"
                fullWidth
              />
            </div>
            <div >
              <FormControlLabel
                control={<Checkbox onClick={checkboxHandler} name="checkbox" />}
                label="Contractor is internal"
              />
            </div>
          </div>
          {checkbox && (
            <div className="subcontractor-area" id="subcontractor-area">
              <h3>Internal contractor information</h3>
              <div >
                <TextField
                  type="text"
                  value={subContractorName}
                  className="subContractorName"
                  name="subContractorName"
                  onChange={changeValueHandler}
                  variant="outlined"
                  label="Contractor name"
                  required={required}
                  fullWidth
                />
              </div>
              <div >
                <TextField
                  type="number"
                  value={price2}
                  className="price2"
                  name="price2"
                  onChange={changeValueHandler}
                  variant="outlined"
                  label="Price"
                  margin="normal"
                  required={required}
                  fullWidth
                />
              </div>
              <div >
                <TextField
                  type="text"
                  value={otherInfo}
                  className="otherInfo"
                  name="otherInfo"
                  onChange={changeValueHandler}
                  variant="outlined"
                  label="Other info"
                  margin="normal"
                  fullWidth
                />
              </div>
            </div>
          )}
          <Button variant="contained" type="submit" color="primary" className="btn-send" disabled={isDisabled} >
            Save
          </Button>
        </form>
      </Card>
      <Modal
        open={open}
        onClose={handleClose}>
        {content}
      </Modal>
    </div>
  );
};

export default Form;
